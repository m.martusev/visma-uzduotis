<?php

use Visma\Cache\PatternCache;
use Visma\Cache\WordCache;
use Visma\Utilities\Container;
use Visma\Utilities\FileIO;
use Visma\Utilities\Logger;
use Visma\Utilities\QueryBuilder;

const OPTIONS = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

$config = require __DIR__ . '/db.config.php';
$container = new Container();

$container[FileIO::class] = fn ($container) => new FileIO();
$container[Logger::class] = fn ($container) => new Logger();
$container[PDO::class] = fn ($container) => new PDO($config['dsn'], $config['username'], $config['password'], OPTIONS);
$container[QueryBuilder::class] = fn ($container) => new QueryBuilder($container[PDO::class]);
$container[WordCache::class] = fn ($container) => new WordCache($container[QueryBuilder::class]);
$container[PatternCache::class] = fn ($container) => new PatternCache($container[QueryBuilder::class]);
