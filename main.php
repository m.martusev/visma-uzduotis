<?php

declare(strict_types=1);

use Visma\Cache\{PatternCache, WordCache};
use Visma\Hyphenation\EnglishHyphenation;
use Visma\Utilities\{Container, FileIO, Logger, QueryBuilder, StringFunctions, Timer};
use Psr\SimpleCache\InvalidArgumentException;
use Visma\Hyphenation\ProxyCacheHyphenation;
use Visma\Hyphenation\ProxyLoggingHyphenation;
use Visma\Hyphenation\TextHyphenator;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/di.config.php';

const HYPHENATION_PATTERN_FILE_NAME = 'tex-hyphenation-patterns.txt';
const TEXT_NOT_FOUND_ERROR = 'Text not found';
const FILE_NAME_NOT_PROVIDED_ERROR = 'File name was not provided';
const FILE_NOT_FOUND_ERROR = 'File was not found';

$container = Container::getInstance();
$logger = $container[Logger::class];

$flag = $argv[1] ?? null;

if (strpos($flag, 'd') !== false) {
    $cache = $container[WordCache::class];
    $patternCache = $container[PatternCache::class];
    $hyphenationPatterns = $patternCache->getMultiple([]);
    $hyphenation = new ProxyCacheHyphenation(new EnglishHyphenation($hyphenationPatterns), $cache);
} else {
    $patternText = $container[FileIO::class]->read(HYPHENATION_PATTERN_FILE_NAME);
    $hyphenationPatterns = StringFunctions::getPatternsFromString($patternText);
    $hyphenation = new EnglishHyphenation($hyphenationPatterns);
}

Timer::start();

switch ($flag) {
    case '-t':
    case '-td':
        $text = $argv[2] ?? '';
        break;
    case '-f':
    case '-fd':
        $text = $container[FileIO::class]->read($argv[2]);
        $outputFilePath = $argv[3] ?? null;
        break;
    case '-i':

        $patternFile = $argv[2] ?? null;
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $container[QueryBuilder::class];
        importPatternsToDatabase($patternFile, $queryBuilder);
        exit(1);
    default:
        showFlagList();

        exit(1);
}

try {
    $hyphenation = new ProxyLoggingHyphenation($hyphenation, $logger);
    $textHyphenator = new TextHyphenator();
    $text = $textHyphenator->hyphenateText($hyphenation, $text);

} catch (Exception $e) {
    $logger->error($e->getMessage());
    echo $e->getMessage() . "\n";

    exit(1);
}

if (isset($outputFilePath)) {
    try {
        $container[FileIO::class]->write($outputFilePath, $text);
    } catch (Exception $e) {
        $logger->error($e->getMessage());
        echo $e->getMessage() . "\n";
    }
} else {
    echo $text . "\n";
}

Timer::stop();
$logger->debug((new DateTime())->format('Y-m-d h:i:s') .' Execution time: ' . Timer::getTime() . ' seconds' . "\n");

function showFlagList()
{
    echo "-t [text] Read text from command line\n";
    echo "-f [file] Read text from file\n";
}

/**
 * @throws InvalidArgumentException
 */
function importPatternsToDatabase(?string $patternFileName, QueryBuilder $queryBuilder): void
{
    global $logger;

    if ($patternFileName == null) {
        $logger->error(FILE_NAME_NOT_PROVIDED_ERROR);
        echo FILE_NAME_NOT_PROVIDED_ERROR . "\n";

        exit(1);
    }

    if (!file_exists($patternFileName)) {
        $logger->error(FILE_NOT_FOUND_ERROR);
        echo FILE_NOT_FOUND_ERROR . "\n";

        exit(1);
    }

    try {
        $patternText = (new FileIO())->read($patternFileName);
        $hyphenationPatterns = StringFunctions::getPatternsFromString($patternText);
        (new PatternCache($queryBuilder))->setMultiple($hyphenationPatterns);
    } catch (InvalidArgumentException $e) {
        $logger->error($e->getMessage());
    }
}
