<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hyphenation</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

<h1 class="pt-4 pl-5"><b>Word List</b></h1>
<hr/>
<div class="jumbotron pt-3 ml-5 mr-5">
<?php

use Visma\Cache\WordCache;
use Visma\Utilities\QueryBuilder;

require __DIR__ . '/../../vendor/autoload.php';
//require __DIR__ . '/../../db.config.php';
$dbConfig = require __DIR__ . '/../../db.config.php';

$db = new PDO($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password']);
$queryBuilder = new QueryBuilder($db);
$wordCache = new WordCache($queryBuilder);

$words = $wordCache->getAll();

foreach ($words as $key => $value) {
    echo "<div class='p-2 m-3 bg-light'> <h3><b> $key </b></h3> <br/> <h4><em> Hyphenation: $value </em> </h4> </div>";
    //echo $word . '<br/>';
}

?>
</div>
</body>

</html>


