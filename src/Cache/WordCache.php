<?php

namespace Visma\Cache;

use Exception;
use PDOException;
use Psr\SimpleCache\CacheInterface;
use Visma\Exception\CacheException;
use Visma\Hyphenation\HyphenationResult;
use Visma\Utilities\QueryBuilder;

class WordCache implements CacheInterface
{
    private QueryBuilder $queryBuilder;

    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @inheritDoc
     */
    public function get($key, $default = null): ?array
    {
        $word = $this->queryBuilder
            ->select('word', ['*'])
            ->where('word', '=', $key)
            ->limit(1)
            ->execute()[0];

        if (!$word) {
            return $default;
        }

        return [$word['word'] => $word['hyphenated_word']];
    }

    /**
     * @inheritDoc
     */
    public function set($key, $value, $ttl = null): bool
    {
        if ($this->has($key)) {
            try {
                $this->queryBuilder->beginTransaction();

                $this->queryBuilder
                    ->update('word', ['hyphenated_word'], [$value])
                    ->where('word', '=', $key)
                    ->execute();

                $this->queryBuilder
                    ->delete('word_pattern')
                    ->where('word', '=', $key)
                    ->execute();

                $this->queryBuilder->commit();
            } catch (PDOException $e){
                $this->queryBuilder->rollBack();
                throw new CacheException();
            }

        } else {
            $this->queryBuilder
                ->insertInto('word', ['word', 'hyphenated_word'])
                ->values([[$key, $value]])
                ->execute();
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete($key): bool
    {
        $this->queryBuilder
            ->delete('word')
            ->where('word', '=', $key)
            ->execute();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $this->queryBuilder
            ->delete('word')
            ->execute();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMultiple($keys, $default = null): iterable
    {
        $wordRows = $this->queryBuilder
            ->select('word', ['word', 'hyphenated_word'])
            ->whereIn('word', (array)$keys)
            ->execute();

        $resultArray = [];

        foreach ($wordRows as $row) {
            $patterns = array_column($this->queryBuilder
                ->select('word_pattern', ['pattern'])
                ->where('word', '=', $row['word'])
                ->execute(), 'pattern');

            $resultArray[$row['word']] = new HyphenationResult($row['word'], $row['hyphenated_word'], $patterns);
        }

        return $resultArray;
    }

    /**
     * @inheritDoc
     */
    public function setMultiple($values, $ttl = null): bool
    {
        if (empty($values)) {
            return true;
        }

        $wordRowsToInsert = [];
        $wordPatternRowsToInsert = [];

        foreach ($values as $value) {

            $wordRowsToInsert[] = [$value->getWord(), $value->getHyphenatedWord()];

            foreach ($value->getMatchedPatterns() as $pattern) {
                $wordPatternRowsToInsert[] = [$value->getWord(), $pattern];
            }
        }

        try{
            $this->queryBuilder->beginTransaction();
            $this->queryBuilder
                ->insertInto('word', ['word', 'hyphenated_word'])
                ->values($wordRowsToInsert)
                ->execute();

            $this->queryBuilder
                ->insertInto('word_pattern', ['word', 'pattern'])
                ->values($wordPatternRowsToInsert)
                ->execute();

            $this->queryBuilder->commit();
        } catch (Exception $e) {
            $this->queryBuilder->rollBack();
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple($keys): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function has($key): bool
    {
        return (bool)$this->queryBuilder
            ->select('word', ['*'])
            ->where('word', '=', $key)
            ->limit(1)
            ->execute();
    }

    public function getAll(): array
    {
        $words = $this->queryBuilder
            ->select('word', ['*'])
            ->execute();

        return array_column($words, 'hyphenated_word', 'word');
    }
}