<?php

namespace Visma\Cache;

use PDO;
use Psr\SimpleCache\CacheInterface;
use Visma\Utilities\QueryBuilder;

class PatternCache implements CacheInterface
{
    private QueryBuilder $queryBuilder;

    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @inheritDoc
     */
    public function get($key, $default = null)
    {
        return array_column($this->queryBuilder
            ->select('word_pattern', ['pattern'])
            ->where('word', '=', $key)
            ->execute(), 'pattern');
    }

    /**
     * @inheritDoc
     */
    public function set($key, $value, $ttl = null): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete($key): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $this->queryBuilder
            ->delete('pattern')
            ->execute();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMultiple($keys, $default = null): iterable
    {
        $rows = $this->queryBuilder
            ->select('pattern', ['*'])
            ->execute();

        return array_column($rows, 'pattern');
    }

    /**
     * @inheritDoc
     */
    public function setMultiple($values, $ttl = null): bool
    {
        if (empty($values)) {
            return true;
        }

        $this->clear();
        $valueArray = [];

        foreach ($values as $value) {
            $valueArray[] = [$value];
        }

        $this->queryBuilder
            ->insertInto('pattern', ['pattern'])
            ->values($valueArray)
            ->execute();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple($keys): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function has($key): bool
    {
        return false;
    }
}