<?php

namespace Visma\Tree;

class TreeNode
{
    private $value;

    public array $branches;

    public function __construct($value, $branches = [])
    {
        $this->value = $value;
        $this->branches = $branches;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
}