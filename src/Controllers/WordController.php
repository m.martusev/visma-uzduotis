<?php

namespace Visma\Controllers;

use Exception;
use Visma\Cache\WordCache;
use Visma\Hyphenation\HyphenationInterface;
use Visma\Hyphenation\ProxyCacheHyphenation;

class WordController
{
    private WordCache $wordCache;
    private HyphenationInterface $hyphenation;

    public function __construct(WordCache $wordCache, ProxyCacheHyphenation $hyphenation)
    {
        $this->wordCache = $wordCache;
        $this->hyphenation = $hyphenation;
    }

    public function get(string $word)
    {
        if (!$this->wordCache->has($word)) {
            http_response_code(404);
            return json_encode(['error' => 'Word does not exist in database']);
        } else {
            $result = $this->wordCache->get($word);
            return json_encode(['word' => $word, 'hyphenatedWord' => $result[$word]]);
        }
    }

    public function getAll()
    {
        $words = $this->wordCache->getAll();
        $resultArray = [];

        foreach ($words as $key => $value) {
            $resultArray[] = ['word' => $key, 'hyphenatedWord' => $value];
        }

        return json_encode($resultArray);
    }

    public function post($request)
    {
        if (isset($request['word'])) {
            $word = $request['word'];

            if ($this->wordCache->has($word)) {
                http_response_code(409);
                return json_encode(['error' => 'Word already exists in database']);
            } else {
                $result = $this->hyphenation->hyphenateWords([$word])[$word]->getHyphenatedWord();
                http_response_code(201);
                return json_encode(['word' => $word, 'hyphenatedWord' => $result]);
            }
        } else {
            http_response_code(400);
            return json_encode(['error' => 'Word not given']);
        }
    }

    public function put($request, $word)
    {
        if (!$word) {
            http_response_code(400);
            return json_encode(['error' => 'Word not provided']);
        } elseif (!isset($request['hyphenatedWord'])) {
            http_response_code(400);
            return json_encode(['error' => 'Correction not provided']);
        } elseif (!$this->wordCache->has($word)) {
            http_response_code(404);
            return json_encode(['error' => 'Word does not exist in database']);
        } else {
            try {
                $correction = $request['hyphenatedWord'];
                $this->wordCache->set($word, $correction);
                return json_encode(['word' => $word, 'hyphenatedWord' => $correction]);
            } catch (Exception $e) {
                return json_encode(['error' => $e->getMessage()]);
            }
        }
    }

    public function delete($word)
    {
        if (isset($word)) {
            if (!$this->wordCache->has($word)) {
                http_response_code(404);
                return json_encode(['error' => 'Word does not exist in database']);
            }

            $this->wordCache->delete($word);
            return json_encode(['message' => 'Word successfully deleted']);
        } else {
            $this->wordCache->clear();
            return json_encode(['message' => 'All words deleted successfully']);
        }
    }
}