<?php

namespace Visma\Utilities;

use Exception;
use PDO;

class QueryBuilder
{
    private PDO $db;
    private string $query = '';

    function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function select(string $table, array $fields): self
    {
        $this->query = 'SELECT ' . implode(', ', $fields) . ' FROM ' . $table . '';

        return $this;
    }

    public function insertInto(string $table, array $fields): self
    {
        $this->query = 'INSERT INTO ' . $table . ' (' . implode(', ', $fields) . ')';

        return $this;
    }

    public function update(string $table, array $fields, array $newValues): self
    {
        $this->query = 'UPDATE ' . $table . ' SET ';

        foreach ($fields as $key => $field) {
            $this->query .= $field . " = '" . $newValues[$key] . "',";
        }

        $this->query = substr($this->query, 0, -1);

        return $this;
    }

    public function delete(string $table): self
    {
        $this->query = 'DELETE FROM ' . $table;

        return $this;
    }

    public function values(array $rowsOfValues)
    {
        $this->query .= ' VALUES ';

        foreach ($rowsOfValues as $key => $row) {

            foreach ($row as $rowKey => $value) {
                $row[$rowKey] = "'" . $value . "'";
            }

            $rowsOfValues[$key] = '(' . implode(', ', $row) . ')';
        }

        $this->query .= implode(', ', $rowsOfValues);

        return $this;
    }


    public function where(string $field, string $operation, string $value): self
    {
        $this->query .= ' WHERE ' . $field . ' ' . $operation . " '" . $value . "'";

        return $this;
    }

    public function whereIn(string $field, array $values): self
    {
        foreach ($values as $key => $value) {
            $values[$key] = "'" . $value . "'";
        }

        $this->query .= ' WHERE ' . $field . ' IN (' . implode(', ', $values) . ')';

        return $this;
    }

    public function limit(int $limit): self
    {
        $this->query .= ' LIMIT ' . $limit;

        return $this;
    }

    public function execute(): array
    {
        try {
            return $this->db->query($this->query)->fetchAll();
        } catch (Exception $e) {
            return [];
        }
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }

    public function commit()
    {
        $this->db->commit();
    }

    public function rollBack()
    {
        $this->db->rollBack();
    }

}