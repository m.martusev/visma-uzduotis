<?php

declare(strict_types=1);

namespace Visma\Utilities;

class Timer
{
    private static float $startTime;
    private static float $endTime;
    private static bool $running = false;

    public static function start(): void
    {
        self::$startTime = microtime(true);
        self::$endTime = 0;
        self::$running = true;
    }

    public static function stop(): void
    {
        self::$endTime = microtime(true);
        self::$running = false;
    }

    public static function getTime(): float
    {
        if (self::$running) {
            return microtime(true) - self::$startTime;
        } else {
            return self::$endTime - self::$startTime;
        }
    }
}