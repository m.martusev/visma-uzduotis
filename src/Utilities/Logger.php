<?php

namespace Visma\Utilities;

use Psr\Log\{AbstractLogger, LogLevel};
use SplFileObject;

class Logger extends AbstractLogger
{
    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = []): void
    {
        switch ($level) {
            case LogLevel::EMERGENCY:
            case LogLevel::ALERT:
            case LogLevel::CRITICAL:
            case LogLevel::ERROR:
            case LogLevel::WARNING:
                $logFile = 'log/errors.txt';
                break;
            case LogLevel::NOTICE:
            case LogLevel::INFO:
            case LogLevel::DEBUG:
            default:
                $logFile = 'log/log.txt';
                break;
        }

        $logFile = new SplFileObject($logFile, 'a+');
        $message .= "\n";
        $logFile->fwrite($message, strlen($message));
    }
}