<?php

namespace Visma\Utilities;

use SplFileObject;
use Visma\Exception\IllegalArgument;

class FileIO
{
    private const INPUT_FILE_NAME_NOT_PROVIDED = 'Input file name was not provided';
    private const INPUT_FILE_NOT_FOUND = 'Input file not found';
    private const OUTPUT_FILE_NAME_NOT_PROVIDED = 'Output file name was not provided';

    /**
     * @throws IllegalArgument
     */
    public function read(string $inputFileName): string
    {
        if ($inputFileName == null) {
           throw new IllegalArgument(self::INPUT_FILE_NAME_NOT_PROVIDED);
        }

        if (!file_exists($inputFileName)) {
            throw new IllegalArgument(self::INPUT_FILE_NOT_FOUND);
        }

        $inputFile = new SplFileObject($inputFileName, 'r');

        return $inputFile->fread($inputFile->getSize());
    }

    /**
     * @throws IllegalArgument
     */
    public function write(string $outputFileName, $text): void
    {
        if ($outputFileName == null) {
            throw new IllegalArgument(self::OUTPUT_FILE_NAME_NOT_PROVIDED);
        }
        //Container::getContainer()[SplFileObject::class];
        $file = new SplFileObject($outputFileName, 'w');
        $file->fwrite($text, strlen($text));
    }
}