<?php

declare(strict_types=1);

namespace Visma\Utilities;

class StringFunctions
{
    public static function stringLeaveOnlyLetters(string $string): string
    {
        return trim(preg_replace('/[\d]/', '', $string));
    }

    public static function stringStartsWith(string $string, string $substring): bool
    {
        return substr($string, 0, strlen($substring)) === $substring;
    }

    public static function stringEndsWith(string $string, string $substring): bool
    {
        return substr($string, -strlen($substring)) === $substring;
    }

    public static function addHyphenation(string $word): string
    {
        return preg_replace(['/[13579]/', '/[02468]/'], ['-', ''], $word);
    }

    public static function getWordsFromString(string $text): array
    {
       if (preg_match_all('/[a-zA-Z]+/', $text, $words) !== false) {
           return $words[0];
       }

       return [];
    }

    public static function getPatternsFromString(string $text): array
    {
        if (preg_match_all('/\.?[a-z0-9]+\.?/', $text, $patterns)) {
            return $patterns[0];
        }

        return [];
    }
}