<?php

namespace Visma\Utilities;

use ArrayAccess;
use Closure;
use Psr\Container\ContainerInterface;
use Visma\Exception\DependencyNotFoundException;

class Container implements ContainerInterface, ArrayAccess
{
    private static array $instances = [];

    /**
     * @inheritDoc
     */
    public function get(string $id)
    {
        if (!$this->has($id)) {
            throw new DependencyNotFoundException($id);
        }

        $entry = self::$instances[$id];

        if ($entry instanceof Closure) {
            return $entry($this);
        }

        return $entry;
    }

    /**
     * @inheritDoc
     */
    public function has(string $id): bool
    {
        return isset(self::$instances[$id]);
    }

    public function set($id, $dependency)
    {
        self::$instances[$id] = $dependency;
    }

    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        unset(self::$instances[$offset]);
    }

    public static function getInstance(): self
    {
        return new static();
    }
}
