<?php

namespace Visma\Exception;

use Exception;
use Psr\SimpleCache\CacheException as PsrCacheException;

class CacheException extends Exception implements PsrCacheException
{
    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}