<?php

namespace Visma\Exception;

use Exception;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;

class DependencyNotFoundException extends Exception implements NotFoundExceptionInterface
{

    public function __construct($dependency, $code = 0, Throwable $previous = null)
    {
        $message = 'Dependency ' . $dependency . ' was not found';
        parent::__construct($message, $code, $previous);
    }
}