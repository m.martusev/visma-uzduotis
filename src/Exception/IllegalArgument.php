<?php

namespace Visma\Exception;

use Exception;
use Psr\SimpleCache\InvalidArgumentException;

class IllegalArgument extends Exception implements InvalidArgumentException
{
    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}