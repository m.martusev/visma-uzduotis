<?php

namespace Visma\Hyphenation;

use Psr\SimpleCache\CacheInterface;

class ProxyCacheHyphenation implements HyphenationInterface
{
    private HyphenationInterface $hyphenation;
    private CacheInterface $cache;

    function __construct(HyphenationInterface  $hyphenation, CacheInterface $cache)
    {
        $this->hyphenation = $hyphenation;
        $this->cache = $cache;
    }


    function hyphenateWords(array $words): array
    {
        $hyphenated = $this->cache->getMultiple($words);
        
        $notHyphenated = array_filter($words, fn(string $word) => empty($hyphenated[$word]));
        $newlyHyphenated = $this->hyphenation->hyphenateWords($notHyphenated);

        $this->cache->setMultiple($newlyHyphenated);

        return array_merge($hyphenated, $newlyHyphenated);
    }
}