<?php

namespace Visma\Hyphenation;

class HyphenationResult
{
    private string $word;
    private string $hyphenatedWord;
    private array $matchedPatterns = [];

    function __construct(string $word, string $hyphenatedWord, array $matchedPatterns)
    {
        $this->word = $word;
        $this->hyphenatedWord = $hyphenatedWord;
        $this->matchedPatterns = $matchedPatterns;
    }

    public function getWord(): string
    {
        return $this->word;
    }

    public function getHyphenatedWord(): string
    {
        return $this->hyphenatedWord;
    }

    public function getMatchedPatterns(): array
    {
        return $this->matchedPatterns;
    }

}