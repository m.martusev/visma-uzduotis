<?php

namespace Visma\Hyphenation;

use Visma\Utilities\StringFunctions;

class TextHyphenator implements TextHyphenatorInterface
{
    public function hyphenateText (HyphenationInterface $hyphenation, string $text)
    {
        $words = StringFunctions::getWordsFromString($text);
        $result = $hyphenation->hyphenateWords($words);

        foreach ($words as $word)
        {
            $text = str_replace($word, $result[$word]->getHyphenatedWord(), $text);
        }

        return $text;
    }
}