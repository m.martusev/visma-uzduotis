<?php

namespace Visma\Hyphenation;

use Psr\Log\LoggerInterface;

class ProxyLoggingHyphenation implements HyphenationInterface
{
    private HyphenationInterface $hyphenation;
    private LoggerInterface $logger;

    function __construct(HyphenationInterface  $hyphenation, LoggerInterface $logger)
    {
        $this->hyphenation = $hyphenation;
        $this->logger = $logger;
    }

    function hyphenateWords(array $words): array
    {
        $hyphenatedWords = $this->hyphenation->hyphenateWords($words);

        foreach ($hyphenatedWords as $word) {
            $this->logger->debug('Word: ' . $word->getWord());
            $this->logger->debug('Hyphenation: ' . $word->getHyphenatedWord());
            $this->logger->debug('Patterns: '. implode(', ', $word->getMatchedPatterns()) . PHP_EOL);
        }

        return $hyphenatedWords;
    }
}