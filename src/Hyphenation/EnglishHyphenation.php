<?php

declare(strict_types=1);

namespace Visma\Hyphenation;

use Visma\Tree\TreeNode;
use Visma\Utilities\StringFunctions;

class EnglishHyphenation implements HyphenationInterface
{
    private TreeNode $patternTree;

    function __construct(array $hyphenationPatterns)
    {
        $this->patternTree = $this->createPatternTree($hyphenationPatterns);
    }

    private function hyphenate(string $word): HyphenationResult
    {
        $hyphenationArrays = [];
        $matchedPatterns = [];
        $dWord = '.' . $word . '.';

        for ($i = 0; $i < strlen($dWord); $i++) {

            $currentNode = $this->patternTree;

            for ($j = $i; $j < strlen($dWord) and $currentNode->branches != null; $j++) {
                $branchToLookFor = $dWord[$j];

                if (isset($currentNode->branches[$branchToLookFor])) {
                    $currentNode = $currentNode->branches[$branchToLookFor];

                    if ($currentNode->getValue() != null) {
                        $matchedPatterns[] = $currentNode->getValue();
                        $hyphenationArrays[] = $this->getHyphenationArray(
                            $i,
                            $currentNode->getValue(),
                            strlen($dWord)
                        );
                    }
                    continue;
                }
                break;
            }
        }

        $reducedArray = $this->reduceArrays(strlen($dWord), $hyphenationArrays);
        $numberedWord = $this->insertHyphenationNumbersIntoWord($dWord, $reducedArray);

        $hyphenatedWord = substr(StringFunctions::addHyphenation($numberedWord), 1, -1);
        return new HyphenationResult($word, $hyphenatedWord, $matchedPatterns);
    }

    public function hyphenateWords(array $words): array
    {
        $result = [];

        foreach ($words as $word) {
            $result[$word] = $this->hyphenate($word);
        }

        return $result;
    }

    private function getHyphenationArray(int $patternStart, string $pattern, int $length): array
    {
        $hyphenationArray = array_fill(0, $length, 0);
        $positionCounter = -1;

        for ($i = 0; $i < strlen($pattern); $i++) {
            if (is_numeric($pattern[$i])) {
                $hyphenationArray[$patternStart + $positionCounter] = $pattern[$i];
            } else {
                $positionCounter++;
            }
        }

        $hyphenationArray[] = $pattern;

        return $hyphenationArray;
    }

    private function reduceArrays(int $wordLength, array $hyphenationArrays): array
    {
        if (sizeof($hyphenationArrays) == 0) {
            return array_fill(0, $wordLength, 0);
        }

        $reducedArray = [];

        for ($i = 0; $i < $wordLength; $i++) {
            $column = array_column($hyphenationArrays, $i);
            $reducedArray[] = max($column);
        }

        return $reducedArray;
    }

    private function insertHyphenationNumbersIntoWord(string $word, array $reducedHyphenationArray): string
    {
        $string = '';
        $reducedHyphenationArray[0] = 0;
        $reducedHyphenationArray[count($reducedHyphenationArray)-2] = 0;

        for ($i = 0; $i < strlen($word); $i++) {
            $string .= $word[$i] . $reducedHyphenationArray[$i];
        }

        return $string;
    }

    private function createPatternTree(array $hyphenationPatterns): TreeNode
    {
        $rootNode = new TreeNode('');

        foreach ($hyphenationPatterns as $pattern) {
            $currentNode = $rootNode;
            $patternSymbols = str_split(StringFunctions::stringLeaveOnlyLetters($pattern));

            foreach ($patternSymbols as $symbol) {
                if (!array_key_exists($symbol, $currentNode->branches)) {
                    $currentNode->branches[$symbol] = new TreeNode('');
                }

                $currentNode = $currentNode->branches[$symbol];
            }

            $currentNode->setValue($pattern);
        }

        return $rootNode;
    }
}