<?php

declare(strict_types=1);

namespace Visma\Hyphenation;

interface HyphenationInterface
{
    function hyphenateWords(array $words): array;
}
