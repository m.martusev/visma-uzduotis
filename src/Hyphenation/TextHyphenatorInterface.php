<?php

namespace Visma\Hyphenation;

interface TextHyphenatorInterface
{
    public function hyphenateText(HyphenationInterface $hyphenation, string $text);
}