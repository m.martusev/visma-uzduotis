<?php

use Visma\Cache\PatternCache;
use Visma\Cache\WordCache;
use Visma\Controllers\WordController;
use Visma\Hyphenation\EnglishHyphenation;
use Visma\Hyphenation\ProxyCacheHyphenation;
use Visma\Utilities\Container;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/di.config.php';

$container = Container::getInstance();

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$patterns = [];

$paths = explode('/', $uri);
array_shift($paths);

if (array_shift($paths) === 'words') {

    try {
        $patterns = $container[PatternCache::class]->getMultiple([]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['error' => 'Internal server error']);
    }

    $wordCache = $container[WordCache::class];
    $hyphenation = new ProxyCacheHyphenation(new EnglishHyphenation($patterns), $wordCache);
    $wordController = new WordController($wordCache, $hyphenation);

    switch ($method) {
        case 'GET':
            $word = array_shift($paths);

            if (isset($word)) {
                echo $wordController->get($word);
            } else {
                echo $wordController->getAll();
            }

            break;

        case 'POST':
            $json = file_get_contents( 'php://input', 'r' );
            $request = json_decode($json, true);
            echo $wordController->post($request);

            break;

        case 'PUT':
            $json = file_get_contents( 'php://input', 'r' );
            $request = json_decode($json, true);
            $word = array_shift($paths);
            echo $wordController->put($request, $word);

            break;
        case 'DELETE':
            $word = array_shift($paths);
            echo $wordController->delete($word);

            break;

        default:
            http_response_code(404);
            echo json_encode(['error' => 'Unhandled method']);
    }
}
