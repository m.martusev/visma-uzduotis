<?php

class InsertWordCest
{
    private const WORD = 'aaaaaabbbbb';

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
        $I->sendDelete('/words/' . self::WORD);
    }

    // tests
    public function tryToTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/words', ['word' => self::WORD]);
        $I->seeResponseCodeIsSuccessful();
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'word' => 'string',
            'hyphenatedWord' => 'string'
        ]);
    }


}
