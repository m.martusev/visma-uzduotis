<?php

namespace Visma\Test\Utilities;

use PHPUnit\Framework\TestCase;
use Visma\Utilities\StringFunctions;

class StringFunctionsTest extends TestCase
{
    /**
     * @dataProvider provideStringLeaveOnlyLetters
     */
    public function testStringLeaveOnlyLetters(string $string, string $expected): void
    {
        $actual = StringFunctions::stringLeaveOnlyLetters($string);
        $this->assertSame($actual, $expected);
    }

    /**
     * @dataProvider provideStringStartsWith
     */
    public function testStringStartsWith(string $string, string $substring, bool $expected)
    {
        $this->assertSame(StringFunctions::stringStartsWith($string, $substring), $expected);
    }

    /**
     * @dataProvider provideStringEndsWith
     */
    public function testStringEndsWith(string $string, string $substring, bool $expected)
    {
        $this->assertSame(StringFunctions::stringEndsWith($string, $substring), $expected);
    }

    /**
     * @dataProvider provideAddHyphenation
     */
    public function testAddHyphenation(string $string, string $expected)
    {
        $this->assertSame(StringFunctions::addHyphenation($string), $expected);
    }

    /**
     * @dataProvider provideGetWordsFromString
     */
    public function testGetWordsFromString(string $text, array $expected)
    {
        $this->assertEquals(StringFunctions::getWordsFromString($text), $expected);
    }

    /**
     * @dataProvider provideGetPatternsFromString
     */
    public function testGetPatternsFromString(string $text, array $expected)
    {
        $this->assertEquals(StringFunctions::getPatternsFromString($text), $expected);
    }

    public function provideStringLeaveOnlyLetters(): array
    {
        return [
            'simple' => ['abc1', 'abc'],
            'onlyLetters' => ['abc', 'abc'],
            'onlyNumbers' => ['132', ''],
        ];
    }

    public function provideStringStartsWith(): array
    {
        return [
            'doesStartWith' => ['123abcde', '123ab', true],
            'doesntStartWith' => ['123abcde', '23abc', false],
            'same' => ['abc', 'abc', true],
            'empty' => ['', '', true],
        ];
    }

    public function provideStringEndsWith(): array
    {
        return [
            'doesEndtWith' => ['123abcde', '23abcde', true],
            'doesntStartWith' => ['123abcde', '123abc', false],
            'same' => ['abc', 'abc', true],
            'empty' => ['', '', true],
        ];
    }

    public function provideAddHyphenation(): array
    {
        return [
            'simple' => ['h2el1lo', 'hel-lo'],
            'onlyLetters' => ['dog', 'dog'],
            'onlyNumbers' => ['12345', '---'],
        ];
    }

    public function provideGetWordsFromString(): array
    {
        return [
            'simple' => ['Hello, goodbye. Raincoat', ['Hello', 'goodbye', 'Raincoat']],
            'hyphens' => ['Vilnius-Kaunas-Klaipeda.', ['Vilnius', 'Kaunas', 'Klaipeda']],
            'numbers' => ['Test123, 1.6 m, 100 kilometers', ['Test', 'm', 'kilometers']],
            'noWords' => ['   1354%$  %$%$ 6 16     ||', []],
            'empty' => ['', []],
        ];
    }

    public function provideGetPatternsFromString(): array
    {
        return [
            'simple' => ['a1dd2; .sd1fs-16gg.h1', ['a1dd2', '.sd1fs', '16gg.', 'h1']],
            'newLines' => ["a1s2f\nndf4fd\nv1d5f.\n.2c", ['a1s2f', 'ndf4fd', 'v1d5f.', '.2c']],
        ];
    }
}