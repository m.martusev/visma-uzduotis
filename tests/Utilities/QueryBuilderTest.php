<?php

namespace Visma\Test\Utilities;

use PDO;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;
use Visma\Utilities\QueryBuilder;

class QueryBuilderTest extends TestCase
{
    const EXPECTED_SELECT = "SELECT word, hyphenated_word FROM words";
    const EXPECTED_INSERT = "INSERT INTO customers (name, phone, email)";
    const EXPECTED_UPDATE = "UPDATE words SET hyphenated_word = 'your-self'";
    const EXPECTED_DELETE = "DELETE FROM customers";
    const EXPECTED_WHERE = " WHERE id = '101'";
    const EXPECTED_WHERE_IN = " WHERE id IN ('1', '2', '10')";
    const EXPECTED_VALUES = " VALUES ('Jonas', '35'), ('Petras', '56')";
    const EXPECTED_LIMIT = " LIMIT 1";

    private QueryBuilder $queryBuilder;
    private ReflectionProperty $property;

    public function setUp(): void
    {
        $mockPDO = $this->createMock(PDO::class);
        $this->queryBuilder = new QueryBuilder($mockPDO);

        $this->property = new ReflectionProperty($this->queryBuilder, 'query');
        $this->property->setAccessible(true);
    }

    public function testSelect()
    {
        $queryBuilder = $this->queryBuilder->select('words', ['word', 'hyphenated_word']);
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_SELECT, $this->property->getValue($this->queryBuilder));
    }

    public function testInsert()
    {
        $queryBuilder = $this->queryBuilder->insertInto('customers', ['name', 'phone', 'email']);
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_INSERT, $this->property->getValue($this->queryBuilder));
    }

    public function testUpdate()
    {
        $queryBuilder = $this->queryBuilder->update('words', ['hyphenated_word'], ['your-self']);
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_UPDATE, $this->property->getValue($this->queryBuilder));
    }

    public function testDelete()
    {
        $queryBuilder = $this->queryBuilder->delete('customers');
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_DELETE, $this->property->getValue($this->queryBuilder));
    }

    public function testWhere()
    {
        $queryBuilder = $this->queryBuilder->where('id', '=', '101');
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_WHERE, $this->property->getValue($this->queryBuilder));
    }

    public function testWhereIn()
    {
        $queryBuilder = $this->queryBuilder->whereIn('id', ['1', '2', '10']);
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_WHERE_IN, $this->property->getValue($this->queryBuilder));
    }

    public function testValues()
    {
        $queryBuilder = $this->queryBuilder->values([['Jonas', '35'],['Petras', '56']]);
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_VALUES, $this->property->getValue($this->queryBuilder));
    }

    public function testLimit()
    {
        $queryBuilder = $this->queryBuilder->limit(1);
        $this->assertSame($this->queryBuilder, $queryBuilder);
        $this->assertSame( self::EXPECTED_LIMIT, $this->property->getValue($this->queryBuilder));
    }

}