<?php

namespace Visma\Test\Hyphenation;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;
use Visma\Cache\WordCache;
use Visma\Hyphenation\EnglishHyphenation;
use Visma\Hyphenation\HyphenationResult;

class EnglishHyphenationTest extends TestCase
{

    private EnglishHyphenation $englishHyphenation;

    /**
     * @var mixed|MockObject|WordCache
     */
    private $mockWordCache;

    public function setUp(): void
    {

        $mockPatterns = ['at.', '1er', 'air1', 'r2ef3', '1ane'];

        $this->mockWordCache = $this->createMock(WordCache::class);

        $this->mockWordCache->method('setMultiple')->willReturn(true);

        $this->englishHyphenation = new EnglishHyphenation($mockPatterns, $this->mockWordCache);
    }

    /**
     * @dataProvider provideHyphenateWords
     */
    public function testHyphenateWords(array $words, array $wordFromCache, array $expected)
    {
        $this->mockWordCache
            ->expects($this->exactly(2))
            ->method('getMultiple')
            ->with($words)
            ->willReturnOnConsecutiveCalls($wordFromCache, $expected)
        ;

        $results = $this->englishHyphenation->hyphenateWords($words);
        $this->assertEquals($expected, $results);

        $results = $this->englishHyphenation->hyphenateWords($words);
        $this->assertEquals($expected, $results);
    }

    /**
     * @dataProvider provideGetHyphenationArray
     */
    public function testGetHyphenationArray($patternStart, $pattern, $length, $expected)
    {
        $method = new ReflectionMethod($this->englishHyphenation, 'getHyphenationArray');
        $method->setAccessible(true);
        $result = $method->invoke($this->englishHyphenation, $patternStart, $pattern, $length);

        $this->assertEquals($expected, $result);
    }

    public function provideHyphenateWords(): array
    {
        return [
            'mixed' => [
                ['you', 'refrigerator', 'airplane'],
                [
                    'you' => new HyphenationResult('you', 'you', []),
                    'refrigerator' => new HyphenationResult('refrigerator', 'refrigera-tor', []),
                ],
                [
                    'you' => new HyphenationResult('you', 'you', []),
                    'refrigerator' => new HyphenationResult('refrigerator', 'refrigera-tor', []),
                    'airplane' => new HyphenationResult('airplane', 'air-pl-ane', ['air1','1ane']),
                ],
            ],
            'empty' => [
                [],
                [],
                [],
            ],
        ];
    }

    public function provideGetHyphenationArray(): array
    {
        return [
            [2, 'a2b3', 7, [0, 0, '2', '3', 0, 0, 0, 'a2b3']],
            [6, 'ab3', 7, [0, 0, 0, 0, 0, 0, 0, '3', 'ab3']],
        ];
    }


}